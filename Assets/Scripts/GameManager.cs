﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static GameManager Instance { set; get;}

	private bool gameIsStarted = false;
	private AudioSource backMusic;

	public Text scoreTxt, coinTxt, speedTxt;

	private PlayerMovement movment;
	private float score, coinScore, speedScore;


	private void Awake(){
		Instance = this;
		speedScore = 1;
		UpdateScores ();
		movment = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerMovement> ();
	}

	public void UpdateScores(){
		scoreTxt.text = "Score : "+score.ToString("0");
		coinTxt.text = "Coins : "+coinScore.ToString ("0");
		speedTxt.text = "Speed : x"+speedScore.ToString ("0.0");
	}

	public void UpdateSpeed(float speedAmount){
	
		speedScore = speedAmount;
		UpdateScores ();
	}

	public void GetCoin(){
		coinScore += 1;
		UpdateScores ();
	}
	// Use this for initialization
	void Start () {

		backMusic = GetComponent<AudioSource> ();
		backMusic.Play ();
		
	}
	
	// Update is called once per frame
	void Update () {
		if (MobileInputs.Instance.Tap && !gameIsStarted) {
			gameIsStarted = true;
			movment.StartRunning ();
		}
		if (gameIsStarted) {
			score += Time.time * 2;
			UpdateScores ();
		}
	}
}

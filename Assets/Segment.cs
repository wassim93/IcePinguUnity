﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segment : MonoBehaviour {

	public int SegId {set;get;}
	public bool transition;
	public int beginY1, beginY2, beginY3;
	public int endY1, endY2, endY3;

	public int lenght;

	private Obstacle[] obstacles;

	public void Awake(){
		obstacles = gameObject.GetComponentsInChildren<Obstacle> ();
	}

	public void Spawn(){
		gameObject.SetActive (true);
	}

	public void DeSpawn(){
		gameObject.SetActive (false);
	}


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

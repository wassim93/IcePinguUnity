﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {

	public ObstacleType type;
	private Obstacle currentObstacle;

	public void SpawnObst(){
	
		currentObstacle = ObstacleManager.Instance.GetObstacle (type,0);
		currentObstacle.gameObject.SetActive (true);
		currentObstacle.transform.SetParent (transform, false);


	}

	public void DespawnObs(){
		currentObstacle.gameObject.SetActive (false);
	}
}
